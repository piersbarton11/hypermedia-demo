package com.taimoor.spring.hateoas.mapper;

import com.taimoor.spring.hateoas.dto.PatientDto;
import com.taimoor.spring.hateoas.dto.PatientDto.PatientDtoBuilder;
import com.taimoor.spring.hateoas.model.Patient;
import com.taimoor.spring.hateoas.model.Patient.PatientBuilder;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-05-01T16:45:12+0100",
    comments = "version: 1.3.0.Final, compiler: javac, environment: Java 11.0.7 (Ubuntu)"
)
public class PatientMapperImpl implements PatientMapper {

    @Override
    public Patient toModel(PatientDto patientDto) {
        if ( patientDto == null ) {
            return null;
        }

        PatientBuilder patient = Patient.builder();

        patient.id( patientDto.getId() );
        patient.name( patientDto.getName() );

        return patient.build();
    }

    @Override
    public PatientDto toDto(Patient patient) {
        if ( patient == null ) {
            return null;
        }

        PatientDtoBuilder patientDto = PatientDto.builder();

        patientDto.id( patient.getId() );
        patientDto.name( patient.getName() );

        return patientDto.build();
    }
}
