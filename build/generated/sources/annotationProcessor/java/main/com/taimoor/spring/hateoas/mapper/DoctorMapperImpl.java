package com.taimoor.spring.hateoas.mapper;

import com.taimoor.spring.hateoas.dto.DoctorDto;
import com.taimoor.spring.hateoas.dto.DoctorDto.DoctorDtoBuilder;
import com.taimoor.spring.hateoas.dto.PatientDto;
import com.taimoor.spring.hateoas.model.Doctor;
import com.taimoor.spring.hateoas.model.Doctor.DoctorBuilder;
import com.taimoor.spring.hateoas.model.Patient;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.mapstruct.factory.Mappers;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-05-01T16:45:13+0100",
    comments = "version: 1.3.0.Final, compiler: javac, environment: Java 11.0.7 (Ubuntu)"
)
public class DoctorMapperImpl implements DoctorMapper {

    private final PatientMapper patientMapper = Mappers.getMapper( PatientMapper.class );

    @Override
    public Doctor toModel(DoctorDto doctorDto) {
        if ( doctorDto == null ) {
            return null;
        }

        DoctorBuilder doctor = Doctor.builder();

        doctor.patientList( patientDtoListToPatientList( doctorDto.getPatientDtoList() ) );
        doctor.id( doctorDto.getId() );
        doctor.name( doctorDto.getName() );
        doctor.speciality( doctorDto.getSpeciality() );

        return doctor.build();
    }

    @Override
    public DoctorDto toDto(Doctor doctor) {
        if ( doctor == null ) {
            return null;
        }

        DoctorDtoBuilder doctorDto = DoctorDto.builder();

        doctorDto.patientDtoList( patientListToPatientDtoList( doctor.getPatientList() ) );
        doctorDto.id( doctor.getId() );
        doctorDto.name( doctor.getName() );
        doctorDto.speciality( doctor.getSpeciality() );

        return doctorDto.build();
    }

    protected List<Patient> patientDtoListToPatientList(List<PatientDto> list) {
        if ( list == null ) {
            return null;
        }

        List<Patient> list1 = new ArrayList<Patient>( list.size() );
        for ( PatientDto patientDto : list ) {
            list1.add( patientMapper.toModel( patientDto ) );
        }

        return list1;
    }

    protected List<PatientDto> patientListToPatientDtoList(List<Patient> list) {
        if ( list == null ) {
            return null;
        }

        List<PatientDto> list1 = new ArrayList<PatientDto>( list.size() );
        for ( Patient patient : list ) {
            list1.add( patientMapper.toDto( patient ) );
        }

        return list1;
    }
}
