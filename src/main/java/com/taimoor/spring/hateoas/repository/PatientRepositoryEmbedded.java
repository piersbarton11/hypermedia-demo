package com.taimoor.spring.hateoas.repository;

import com.taimoor.spring.hateoas.dto.PatientDto;
import org.springframework.stereotype.Component;

/**
 * @author Taimoor Choudhary
 */
@Component
public class PatientRepositoryEmbedded implements PatientRepository {

    @Override
    public PatientDto getPatientById(int id) {

        String patientName;

        switch (id) {
            case 1:
                patientName = "J. Smalling";
                break;
            case 2:
                patientName = "Samantha Williams";
                break;
            case 3:
                patientName = "Alfred Tim";
                break;
            case 4:
                patientName = "K. Oliver";
                break;
            default:
                patientName = "Samuel Bradford";
                break;
        }

        return createPatient(id, patientName);
    }

    public PatientDto createPatient(int id, String name){

        return PatientDto.builder()
                .id(id)
                .name(name)
                .build();
    }
}
